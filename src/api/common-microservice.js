'use strict'

const fp = require('fastify-plugin')

// Register all endpoints that should run in one 'common' docker container

module.exports = async function (fastify, opts) {
  fastify.register(fp(async function (fastify, opts) {
    fastify.register(require('./dummy'))
  }))
}
