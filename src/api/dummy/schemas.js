'use strict'

const dummy = {
  schema: {
    response: {
      200: {
        type: 'object',
        require: [ 'hello' ],
        properties: {
          hello: { type: 'string' }
        },
        additionalProperties: false
      }
    }
  }
}

module.exports = {
  dummy
}
