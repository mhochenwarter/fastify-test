'use strict'

const {
  dummy: dummySchema
} = require('./schemas')

module.exports = async function (fastify, opts) {
  // Finally we're registering out routes
  fastify.register(registerRoutes)
}

async function registerRoutes (fastify, opts) {
  // registering login defining the input schema and the output schema
  // See ./schemas.js
  fastify.get('/dummy', dummySchema, async function (req, reply) {
    return {hello: 'Martin'}
  })
}
