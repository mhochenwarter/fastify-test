'use strict'

const path = require('path')
const fastify = require('fastify')()
const helmet = require('fastify-helmet')

const PORT = 3001

const swaggerOption = {
  swagger: {
    info: {
      title: 'Test swagger',
      description: 'testing the fastify swagger api',
      version: '0.1.0'
    },
    host: `localhost:${PORT}`,
    schemes: ['http'],
    consumes: ['application/json'],
    produces: ['application/json']
  },
  exposeRoute: true
}

fastify
.register(helmet)
.register(require('fastify-swagger'), swaggerOption)
.register(require('./api/user'), { prefix: '/api/user' })
.register(require('./api/tweet'), { prefix: '/api/tweet' })
.register(require('./api/follow'), { prefix: '/api/follow' })
.register(require('./api/timeline'), { prefix: '/api/timeline' })
.register(require('./api/dummy'), { prefix: '/api/dummy' })
.register(require('fastify-static'), {
  root: path.join(__dirname, 'frontend', 'build'),
  prefix: '/'
})

fastify.ready(err => {
  if (err) throw err
  fastify.swagger()
})

fastify.listen(PORT, err => {
  if (err) throw err
  console.log(`Server listenting on http://localhost:${fastify.server.address().port}`)
})
